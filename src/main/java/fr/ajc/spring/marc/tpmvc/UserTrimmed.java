package fr.ajc.spring.marc.tpmvc;

import java.util.Date;

public class UserTrimmed {

	private long id;
	private String raisonSociale;
	private UserDataAdministrative userDataAdministrative;
	private int nombreSalaries;
	private boolean insriptionNewsletter;
	private Date dateInscription = new Date();


	public UserTrimmed(String raisonSociale, UserDataAdministrative userDataAdministrative, int nombreSalaries, boolean insriptionNewsletter, Date dateInscription) {
		this.raisonSociale = raisonSociale;
		this.userDataAdministrative = userDataAdministrative;
		this.nombreSalaries = nombreSalaries;
		this.insriptionNewsletter = insriptionNewsletter;
		this.dateInscription = dateInscription;
	}
	
	public UserTrimmed(User user) {
		this.id = user.getId();
		this.raisonSociale = user.getRaisonSociale();
		this.userDataAdministrative = user.getUserDataAdministrative();
		this.nombreSalaries = user.getNombreSalaries();
		this.insriptionNewsletter = user.isInsriptionNewsletter();
		this.dateInscription = user.getDateInscription();
	}

	public UserTrimmed() {
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getRaisonSociale() {
		return raisonSociale;
	}


	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	
	public UserDataAdministrative getUserDataAdministrative() {
		return userDataAdministrative;
	}


	public void setUserDataAdministrative(UserDataAdministrative userDataAdministrative) {
		this.userDataAdministrative = userDataAdministrative;
	}


	public int getNombreSalaries() {
		return nombreSalaries;
	}


	public void setNombreSalaries(int nombreSalaries) {
		this.nombreSalaries = nombreSalaries;
	}


	public boolean isInsriptionNewsletter() {
		return insriptionNewsletter;
	}


	public void setInsriptionNewsletter(boolean insriptionNewsletter) {
		this.insriptionNewsletter = insriptionNewsletter;
	}


	public Date getDateInscription() {
		return dateInscription;
	}


	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}


	public String toString() {
		return "User [id=" + id + ", raisonSociale=" + raisonSociale
				+ ", userDataAdministrative=" + userDataAdministrative + ", nombreSalaries=" + nombreSalaries
				+ ", insriptionNewsletter=" + insriptionNewsletter + ", dateInscription=" + dateInscription + "]";
	}

}
