package fr.ajc.spring.marc.tpmvc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

	@Autowired
	private UserRepository userRepository;



	@RequestMapping(produces = { "application/json", "application/xml" }, method = RequestMethod.GET)
	public @ResponseBody List<UserTrimmed> getUsers(@RequestParam(value = "sort", required = false) String sort, @RequestParam(value = "order", required = false) String order) {
		
		List<User> list =(List<User>) userRepository.findAll();
		List<UserTrimmed> trim = new ArrayList<UserTrimmed>();
		
		if (sort != null) {
			
			switch (sort) {
				case "byDate":
					if (order.equals("desc")) {
						list = userRepository.findAllByOrderByDateInscriptionDesc();
					} else {
						list = userRepository.findAllByOrderByDateInscriptionAsc();
					}
					break;
	
				case "byEmail":
					if (order.equals("desc")) {
						list = userRepository.findAllByOrderByEmailDesc();
					} else {
						list = userRepository.findAllByOrderByEmailAsc();
					}
					break;
		
			}

		}
		
		for (User user : list) {
			trim.add(new UserTrimmed(user));
		}
		
		return trim;
		
	}

	@RequestMapping(value = "/user", produces = { "application/json", "application/xml" }, method = RequestMethod.GET)
	public @ResponseBody User getUserByEmail(@RequestParam String email, HttpServletResponse response) throws IOException {
		
		User user = userRepository.findByEmail(email);
	
		if (user == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			response.sendError(404, "Erreur : Cet utilisateur est introuvable");
		}
		return user;
		
	}



	@PostMapping
	@ResponseStatus(HttpStatus.ACCEPTED)
	public User create(User user) {
		return userRepository.save(user);
	}	
	
}
