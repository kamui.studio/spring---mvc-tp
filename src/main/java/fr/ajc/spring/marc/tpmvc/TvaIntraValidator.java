package fr.ajc.spring.marc.tpmvc;

import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TvaIntraValidator implements ConstraintValidator<TvaIntraConstraint, UserDataAdministrative> {

	private String numSiret;
	private String tvaIntra;
	
	@Override
	public void initialize(TvaIntraConstraint constraintAnnotation) {
		this.numSiret = constraintAnnotation.numSiret();
        this.tvaIntra = constraintAnnotation.tvaIntra();
	}
	
	@Override
	public boolean isValid(UserDataAdministrative userDataAdministrative, ConstraintValidatorContext cxt) {

		// Définition des variables
		String numSiret = userDataAdministrative.getNumSiret();
		String tvaIntra = userDataAdministrative.getTvaIntra();
		List<String> paysTVA = Arrays.asList("AT", "BE", "BG", "CY", "CZ", "DE", "DK", "EE", "EL", "ES", "FI", "FR", "GB", "HR", "HU", "IE", "IT", "LT", "LU", "LV", "MT", "NL", "PL", "PT", "RO", "SE", "SI", "SK");
		boolean isValid = false;

		// Traitements sur le numéro Siret
		int siren = Integer.parseInt(numSiret.substring(5));
		int cle = ((12 + 3 * (siren % 97)) % 97);

		// Traitements sur le numéro de TVA intracommunautaire
		String pays = tvaIntra.substring(0, 2);
		int srn = Integer.parseInt(tvaIntra.substring(4));
		int key = Integer.parseInt(tvaIntra.substring(2, 4));

		// Comparaison du siren et de l'intra
		// ainsi que de la cle calculée à partie du siren et de la cle de l'intra
		// et que le pays fait partie de la liste des pays autorisés
		if (srn == siren && cle == key && paysTVA.contains(pays)) {
			isValid = true;
		}

		return isValid;

	}
	
}
