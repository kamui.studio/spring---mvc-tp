package fr.ajc.spring.marc.tpmvc;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

	User findByEmail(String email);
	List<User> findAllByOrderByEmailAsc();
	List<User> findAllByOrderByEmailDesc();
	List<User> findAllByOrderByDateInscriptionAsc();
	List<User> findAllByOrderByDateInscriptionDesc();
	
}
