package fr.ajc.spring.marc.tpmvc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class EShopController {
	
	@Value("${spring.application.name}")
	String appName;
	
	@GetMapping("/")
	public String homePage(Model m) {
		m.addAttribute("appName", appName);
		return "home";
	}	
	
}
