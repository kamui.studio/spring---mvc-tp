package fr.ajc.spring.marc.tpmvc;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserRepository userRepository;
	

	
	@GetMapping("/subscribe")
	public String subscribe(User user, Model m) {
		return "subscribe";
	}
	
	@GetMapping("/success")
	public String success(Model m) {
		return "success";
	}
	
	@GetMapping("/error-404")
	public String notFound(Model m) {
		return "error-404";
	}



	@PostMapping("/subscribe")
	public String create(@Valid User user, BindingResult bindingResult, HttpServletResponse response) {

		new TvaIntraValidator().isValid(user.getUserDataAdministrative(), bindingResult);
		
		if (bindingResult.hasErrors()) {
			response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
			return "subscribe";
		}

		response.setStatus(HttpServletResponse.SC_CREATED);
		userRepository.save(user);
		return "redirect:success";
	}
	
	
}
