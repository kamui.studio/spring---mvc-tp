package fr.ajc.spring.marc.tpmvc;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@TvaIntraConstraint(
	numSiret = "numSiret", 
	tvaIntra = "tvaIntra", 
	message = "Woooooooooooooooooooooot"
)

public class UserDataAdministrative {

	@Column(nullable = false)
	@NotEmpty(message="Numéro siret obligatoire.")
	@Size(min=14, max=14, message="Numéro siret invalide.")
	private String numSiret;
	
	@Column(nullable = true)
	private String tvaIntra;


	public UserDataAdministrative(String numSiret, String tvaIntra) {
		this.numSiret = numSiret;
		this.tvaIntra = tvaIntra;
	}


	public UserDataAdministrative() {
	}


	public String getNumSiret() {
		return numSiret;
	}


	public void setNumSiret(String numSiret) {
		this.numSiret = numSiret;
	}


	public String getTvaIntra() {
		return tvaIntra;
	}


	public void setTvaIntra(String tvaIntra) {
		this.tvaIntra = tvaIntra;
	}


	public String toString() {
		return "UserDataAdministrative [numSiret=" + numSiret + ", tvaIntra=" + tvaIntra + "]";
	}	
	
}
