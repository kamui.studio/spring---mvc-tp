package fr.ajc.spring.marc.tpmvc;

import javax.validation.Constraint;

@Constraint(validatedBy = TvaIntraValidator.class)

public @interface TvaIntraConstraint {

	String message() default "Numéro de TVA intra. invalide";
	
	String numSiret();
	
	String tvaIntra();
	
}
